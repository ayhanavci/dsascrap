using System;
namespace EnjoyAlgorithms.Module15;

//https://www.enjoyalgorithms.com/blog/maximum-subarray-sum
internal class MaximumSubarraySum
{
    int FindMaxSubArraySumKadanesAlgorithm(int[] nums)
    {
        int maxSumSoFar = nums[0];
        int maxSumEndingHere = nums[0];

        for (int i = 1; i < nums.Length; ++i)
        {
            maxSumEndingHere = Math.Max(maxSumEndingHere + nums[i], nums[i]);
            maxSumSoFar = Math.Max(maxSumEndingHere, maxSumSoFar);
        }
        return maxSumSoFar;
    }
    int FindMaxSubArraySumDynamic(int[] nums)
    {
        int[] maxSumEnding = new int[nums.Length];
        maxSumEnding[0] = nums[0];

        int maxSubArraySum = int.MinValue;
        for (int i = 1; i < nums.Length; ++i)
        {
            if (maxSumEnding[i-1] > 0)
                maxSumEnding[i] = nums[i] + maxSumEnding[i-1];
            else
                maxSumEnding[i] = nums[i];
            
            maxSubArraySum = Math.Max(maxSubArraySum, maxSumEnding[i]);
        }

        return maxSubArraySum;
    }
    int FindMaxSubArraySumMergeSort(int[] nums)
    {
        return 0;
    }

    int FindMaxSubArraySumBrute2(int[] nums)
    {        
        int maxSum = Int32.MinValue;        

        for (int i = 0; i < nums.Length; ++i)
        {    
            int currentSum = 0;        
            for (int j = i; j < nums.Length; ++j)
            {                
                currentSum += nums[j];                    
                maxSum = Math.Max(currentSum, maxSum);
            }
        }

        return maxSum;
    }
    int FindMaxSubArraySumBrute1(int[] nums)
    {        
        int maxSum = Int32.MinValue;        

        for (int i = 0; i < nums.Length; ++i)
        {            
            for (int j = i; j < nums.Length; ++j)
            {
                int currentSum = 0;
                for (int k = i; k <= j; ++k)                
                    currentSum += nums[k];
                maxSum = Math.Max(currentSum, maxSum);
            }
        }

        return maxSum;
    }

    public void Test()
    {
        int[] nums1 = {-4, 5, 7, -6, 10, -15, 3}; //16
        var retVal1 = FindMaxSubArraySumDynamic(nums1);
    }
}