namespace EnjoyAlgorithms.Module15;

internal class MinimumNumberOfJumpsToReachTheEnd
{

    int MinJumps2(int[] A)
    {        
        int []jump = new int[A.Length];
        jump[0] = 0;
        Array.Fill(jump, int.MaxValue);

        for (int i = 1; i < A.Length; ++i)
        {            
            for (int j = 0; j < i; ++j)
            {
                if (j + A[j] >= i && jump[j] != int.MaxValue)
                    jump[i] = Math.Min(jump[i], jump[j] + 1);
            }
        }

        return jump[A.Length-1];
    }
    int MinJumps1(int[] A, int start)
    {
        if (start >= A.Length) return 0;
        int minJumpCount = Int32.MaxValue;

        for (int i = 1; i <= A[start] && i < A.Length; ++i)
        {
            int jumpCount = 1 + MinJumps1(A, start+i);
            minJumpCount = Math.Min(jumpCount, minJumpCount);
        }
        return minJumpCount;
    }
    public void Test()
    {
        int []A = {1, 3, 5, 8, 10, 2, 6, 7, 6, 8, 9};
        var retVal = MinJumps1(A, 0);
    }
}