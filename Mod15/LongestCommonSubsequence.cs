namespace EnjoyAlgorithms.Module15;
using System.Diagnostics;


//https://www.enjoyalgorithms.com/blog/longest-common-subsequence
internal class LongestCommonSubsequence
{
    public int LCS1(string x, string y, int m, int n)
    {
        if (m == 0 || n == 0)
            return 0;
        if (x[m-1] == y[n-1])        
            return 1 + LCS1(x, y, m-1, n-1);
        else 
            return Math.Max(LCS1(x, y, m-1, n), LCS1(x, y, m, n-1));
    }
    int[][] memo;
    public int LCS2(string x, string y, int m, int n) //Top down
    {
        if (m == 0 || n == 0)
            return 0;
                
        if (x[m-1] == y[n-1]) 
        {
            if (memo[m-1][n-1] == -1)
                memo[m-1][n-1] = LCS2(x, y, m-1, n-1);
            
            return 1 + memo[m-1][n-1];
        }            
        else 
        {
            if (memo[m-1][n] == -1)
                memo[m-1][n] = LCS2(x, y, m-1, n);
            
            if (memo[m][n-1] == -1)
                memo[m][n-1] = LCS2(x, y, m, n-1);

            return Math.Max(memo[m-1][n], memo[m][n-1]);
        }
            
    }
    public int LCS21(string x, string y, int m, int n)
    {
        if (m == 0 || n == 0) return 0;
        
        if (memo[m][n] == -1)
        {
            if (x[m-1] == y[n-1])            
                memo[m][n] = 1 + LCS21(x, y, m-1, n-1);
            else
                memo[m][n] = Math.Max(LCS21(x, y, m-1, n), LCS21(x, y, m, n-1));
            
        }

        return memo[m][n];
    }

    public int LCS3(string x, string y, int m, int n) //bottom up
    {    
        InitMemo3(m, n);        
        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (x[i-1] == y[j-1])
                    memo[i][j] = 1 + memo[i-1][j-1];
                else
                    memo[i][j] = Math.Max(memo[i-1][j], memo[i][j-1]);
            }
        }
        return memo[m][n];
    }

    public void InitMemo2(int m, int n)
    {
        Stopwatch timer2 = new Stopwatch();
        timer2.Start();
        memo = new int[m+1][];
        for (int i = 0; i < m+1; ++i) 
        {
            memo[i] = new int[n+1];
            for (int j = 0; j < n + 1; ++j)            
                memo[i][j] = -1;
            
        }        
        timer2.Stop();
        Console.WriteLine("Array fill elapsed time:[{0}]", timer2.ElapsedTicks);
    }
    public void InitMemo3(int m, int n)
    {        
        Stopwatch timer2 = new Stopwatch();
        timer2.Start();
        memo = new int[m+1][];
        for (int i = 0; i < m+1; ++i) 
        {
            memo[i] = new int[n+1];                        
        }        
        timer2.Stop();
        Console.WriteLine("Array fill elapsed time:[{0}]", timer2.ElapsedTicks);
    }
    public void Test()
    {
        string x = "abcbdabddabeeaassddbad";
        string y = "bdcababbaddasdavbddaae";
        int m = x.Length;
        int n = y.Length;
        
        Stopwatch timer1 = new Stopwatch();
        timer1.Start();
        //var retVal1 = LCS1(x, y, x.Length, y.Length);
        timer1.Stop();
        Console.WriteLine("LCS1 elapsed time:[{0}]", timer1.ElapsedTicks);                
        
        InitMemo2(m, n);
        Stopwatch timer2 = new Stopwatch();
        timer2.Start();
        var retVal2 = LCS2(x, y, m, n);
        timer2.Stop();
        Console.WriteLine("LCS2 elapsed time:[{0}]", timer2.ElapsedTicks);
        
        Stopwatch timer3 = new Stopwatch();
        timer3.Start();
        var retVal3 = LCS3(x, y, m, n);
        timer3.Stop();
        Console.WriteLine("LCS3 elapsed time:[{0}]", timer3.ElapsedTicks);
    }
}