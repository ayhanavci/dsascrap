namespace EnjoyAlgorithms.Module15;
internal class ClimbingStairs
{
    int climb3(int n)
    {
        if (n == 0 || n == 1) return n;

        int first = 1;
        int second = 2;        

        for (int i = 3; i <= n; ++i)      
        {
            int current = first + second;
            first = second;
            second = current;            
        }
        return second;
    }

    int climb2(int n)
    {
        int[] ladders = new int[n+1];
        ladders[1] = 1;
        ladders[2] = 2;

        for (int i = 3; i <= n; ++i)        
            ladders[i] = ladders[i-1] + ladders[i-2];
        
        return ladders[n];
    }

    int climb1(int n)
    {        
        if (n == 1) return 1;
        if (n == 2) return 2;

        return climb1(n-1) + climb1(n-2);
    }

    public void Test()
    {
        var retVal = climb3(5);
    }
}