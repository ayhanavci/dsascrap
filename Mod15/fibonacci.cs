using System.Diagnostics;
namespace Teknik.DynamicProgramming;
//https://www.enjoyalgorithms.com/blog/introduction-to-dynamic-programming
internal class Fibonacci
{
    public int Fibonacci1(int n)
    {
        if (n <= 1) return n;
        return Fibonacci1(n - 1) + Fibonacci1(n - 2);
    }

    int[] memo;
    public int Fibonacci2(int n) //Top down - memoization
    {
        if (n <= 1) return n;
        if (memo[n] == -1) memo[n] = Fibonacci2(n - 1) + Fibonacci2(n - 2);
        return memo[n];
    }

    public int Fibonacci3(int n) //Bottom up
    {
        int[] F = new int[n+1];
        F[0] = 0;
        F[1] = 1;
        for (int i = 2; i <= n; ++i)
            F[i] = F[i - 1] + F[i - 2];
        return F[n];
    }

    public void Test()
    {
        int number = 10000;

        Stopwatch timer1 = new Stopwatch();
        timer1.Start();
        //var retVal1 = Fibonacci1(number);
        timer1.Stop();
        Console.WriteLine("Fibo 1 elapsed time:[{0}]", timer1.ElapsedTicks);

        
        //Array fill with def value. Buna gerek yok.
        Stopwatch timer2 = new Stopwatch();
        timer2.Start();
        memo = new int[number+1];        
        Array.Fill(memo, -1);
        timer2.Stop();
        Console.WriteLine("Array Fill elapsed time:[{0}]", timer2.ElapsedTicks);


        Stopwatch timer3 = new Stopwatch();
        timer3.Start();
        var retVal2 = Fibonacci2(number);
        timer3.Stop();
        Console.WriteLine("Fibo 2 Top Down elapsed time:[{0}]", timer3.ElapsedTicks);

        Stopwatch timer4 = new Stopwatch();
        timer4.Start();
        var retVal3 = Fibonacci3(number);
        timer4.Stop();
        Console.WriteLine("Fibo 3 Bottom Up elapsed time:[{0}]", timer4.ElapsedTicks);
    }
}