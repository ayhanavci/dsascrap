namespace EnjoyAlgorithms.Module6;

internal class MaxConsecutiveOnes
{
    int maxOnes(int []nums, int k)
    {            
        int maxOnes = 0 ;
        int zeros = 0;
        int start = 0;
        
        for (int i = 0; i < nums.Length; ++i)
        {
            if (nums[i] == 0) zeros++;

            if (zeros > k)
            {
                if (nums[start] == 0) zeros--;
                start++;
            }
            maxOnes = Math.Max(maxOnes, i - start + 1);
        }
        return maxOnes;
    }
    public void Test()
    {
        int []nums1 = {1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1};
        maxOnes(nums1, 2);
    }
}
