namespace EnjoyAlgorithms.Module6;

internal class LongestSubstringWORepeatChars
{
    int longestSubstring(string str)
    {
        int maxLen = 0;
        bool []visited = new bool[256]; 

        for (int i = 0, j = 0; i < str.Length && j < str.Length;)
        {            
            if (visited[str[j]] == false)
            {
                visited[str[j]] = true;
                j++;
                maxLen = Math.Max(maxLen, j - i + 1);
            }
            else
            {
                visited[str[i]] = false;
                i++;
            }            
            
        }
        return maxLen;
    }
    int longestSubstring2(string str)
    {
        int maxLen = 0;
        for (int i = 0; i < str.Length - 1; ++i)
        {
            bool []visited = new bool[256]; 
            for (int j = i; j < str.Length && visited[str[j]] == false; ++j)
            {
                visited[str[j]] = true;
                maxLen = Math.Max(maxLen, j - i + 1);
            }            
        }
        return maxLen;
    }
    int longestSubstring1(string str)
    {
        int maxLen = 0;
        for (int i = 0; i < str.Length - 1; ++i)
        {
            for (int j = i; j < str.Length; ++j)
            {
                if (!checkUniqueSubstring(str, i, j))
                    break;
                maxLen = Math.Max(maxLen, j - i + 1);
            }
        }
        return maxLen;
    }
    bool checkUniqueSubstring(string str, int i, int j)
    {            
         bool []visited = new bool[256];

         for (int k = i; k <= j; ++k)
         {
            if (visited[str[k]]) return false;
            visited[str[k]] = true;
         } 
         return true;
    }
    public void Test()
    {        
        longestSubstring("abcbbcab");
        longestSubstring("bbbbb");
    }
}
