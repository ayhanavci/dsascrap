using System.Collections;

namespace EnjoyAlgorithms.Module6;

internal class CountDistinctElements
{
    int[] countDistinct(int[] arr, int K)
    {
        List<int> retVal = new List<int>();
        // Creates an empty hashMap hM
        Dictionary<int, int> hM = new Dictionary<int, int>();
 
        // initialize distinct element count for current window
        int dist_count = 0;
 
        // Traverse the first window and store count of every element in hash map
        for (int i = 0; i < K; i++) {
            if (!hM.ContainsKey(arr[i])) {
                hM.Add(arr[i], 1);
                dist_count++;
            }
            else 
                hM[arr[i]]++; 
        } 
        retVal.Add(dist_count);
 
        // Traverse through the remaining array
        for (int i = K; i < arr.Length; i++) {
 
            // Remove first element of previous window.
            // If there was only one occurrence, then reduce distinct count.
            if (hM[arr[i - K]] == 1) {
                hM.Remove(arr[i - K]);
                dist_count--;
            }
            else // reduce count of the removed element                            
                hM[arr[i - K]]--;            
 
            // Add new element of current window
            // If this element appears first time, increment distinct element count
            if (!hM.ContainsKey(arr[i])) {
                hM.Add(arr[i], 1);
                dist_count++;
            }
            else // Increment distinct element count                            
                hM[arr[i]]++;                         
            retVal.Add(dist_count);
        }
        return retVal.ToArray();
    }
    int[] countDistinct1(int []nums, int k)
    {            
        int len = nums.Length;
        int[] retval = new int[len - k + 1];        
        
        for (int i = 0; i <= len - k; ++i)
        {
            int distCount = 0;

            for (int j = i; j < i + k; ++j)
            {
                distCount++;
                for (int l = i; l < j; ++l)
                {
                    if (nums[j] == nums[l])
                    {
                        distCount--;
                        break;
                    }
                }
            }
            retval[i] = distCount;
        }
        return retval;
    }
    public void Test()
    {
        int []nums1 = {1, 1, 1,  3, 4, 2, 3};
        countDistinct(nums1, 4);
    }
}
