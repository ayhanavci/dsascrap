namespace EnjoyAlgorithms.Module6;

internal class MoveZerosToEnd
{
    void Sort(int []nums)
    {            
        int j = 0;

        for (int i = 0; i < nums.Length; ++i)        
            if (nums[i] != 0) nums[j++] = nums[i];            
        
        while (j < nums.Length)        
            nums[j++] = 0;
        
    }
    public void Test()
    {
        int []nums1 = {0, 1, 5, 0, -4, 3, 0};
        Sort(nums1);
    }
}
