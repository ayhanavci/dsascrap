namespace EnjoyAlgorithms.Module6;

internal class FindNRepeatedElement
{
    int findElement2(int []nums)
    {            
       return 0;
    }
    int findElement1(int []nums)
    {            
        int len = nums.Length;
        int n = len / 2;

        Dictionary<int, int> set = new Dictionary<int, int>();

        for (int i = 0; i < len; ++i)        
            if (set.Keys.Contains(nums[i])) set[nums[i]]++;            
            else set.Add(nums[i], 1);
        
        int retVal = -1;
        foreach (var pair in set)
            if (pair.Value == n) 
            {
                retVal = pair.Key; 
                break;
            }
        return retVal;
    }
    public void Test()
    {
        int []nums1 = {1, 2, 2, 3};
        int retVal1 = findElement1(nums1);

        int []nums2 = {2, 1, 2, 5, 3, 2, 2, 4};
        int retVal2 = findElement1(nums2);

        int []nums3 = {5, 1, 5, 2, 5, 3, 5, 4, 6, 5};
        int retVal3 = findElement1(nums3);
    }
}
