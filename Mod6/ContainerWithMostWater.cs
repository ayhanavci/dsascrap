namespace EnjoyAlgorithms.Module6;

internal class ContainerWithMostWater
{
    int findMostWater(int []nums)
    {            
        int maxArea = 0;
        int i = 0, j = nums.Length - 1;
        
        while (i < j)
        {
            int area = (j - i) * Math.Min(nums[i], nums[j]);
            maxArea = Math.Max(maxArea, area);
            if (nums[j] > nums[i]) i++;
            else j--;
        }

        return maxArea;        
    }
    public void Test()
    {
        int []nums1 = {1, 5, 6, 3, 4, 2};
        findMostWater(nums1);
    }
}
