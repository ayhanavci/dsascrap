namespace EnjoyAlgorithms.Module6;

internal class RemoveDuplicateFromSortedArray
{
    int Remove(int []nums)
    {            
        int len = nums.Length;                                
        int uniqueIndex = 0;

        for (int i = 1; i < nums.Length; ++i)    
        {
            if (nums[i] > nums[uniqueIndex])                            
                nums[++uniqueIndex] = nums[i];
            
        }    
                
        return uniqueIndex + 1;        
    }
   
    public void Test()
    {
        int []nums1 = {2, 2, 2, 2, 2, 3, 3, 3};
        int []nums2 = {1, 2, 2, 3, 4, 4, 4, 5, 5};
        int count = Remove(nums2);
    }
}
