namespace EnjoyAlgorithms.Module6;

internal class RainWaterTrapping
{
    int rainWaterTrapping(int []height)
    {            
        int len = height.Length;
        if (len <= 2) return 0;

        int trappedWater = 0;
        int []leftMax = new int[len];
        int []rightMax = new int[len];
        
        leftMax[0] = height[0];
        for (int i = 1; i < len; ++i)
            leftMax[i] = Math.Max(leftMax[i - 1], height[i]);

        rightMax[len - 1] = height[len - 1];
        for (int i = len - 2; i >= 0; --i)
            rightMax[i] = Math.Max(rightMax[i + 1], height[i]);

        for (int i = 0; i < len; ++i)                   
            trappedWater += Math.Min(leftMax[i], rightMax[i]) - height[i];    
        
        return trappedWater;
    }
    int rainWaterTrapping2(int []height)
    {            
        int len = height.Length;
        if (len <= 2) return 0;

        int trappedWater = 0;

        for (int i = 0; i < len; ++i)
        {
            int leftMax = 0;
            for (int j = i; j >= 0; --j)
                leftMax = Math.Max(height[j], leftMax);

            int rightMax = 0;
            for (int j = i; j < len; ++j)
                rightMax = Math.Max(height[j], rightMax);

            trappedWater += Math.Min(leftMax, rightMax) - height[i];    
        }

        return trappedWater;
    }
    public void Test()
    {
        int []nums1 = {1, 5, 6, 3, 4, 2};
        rainWaterTrapping(nums1);
    }
}
