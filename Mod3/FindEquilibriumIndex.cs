namespace EnjoyAlgorithms.Module3;

internal class FindEquilibriumIndex
{
    int findEquilibriumIndex(int []nums)
    {            
        int total = 0;
        for (int i = 0; i < nums.Length; ++i)
            total += nums[i];
        
        int left = 0;
        for (int i = 0; i < nums.Length; ++i)
        {                                                
            int right = total - left - nums[i];            
            if (right == left) return i;
            left += nums[i];            
        }

        return -1;
    }
    public void Test()
    {
        int []nums1 = {-7, 1, 5, 2, -4, 3, 0};
        var retVal1 = findEquilibriumIndex(nums1);

        int []nums2 = {0, 1, 3, -2, -1};
        var retVal2 = findEquilibriumIndex(nums2);

        int []nums3 = {1, 2, -2, -1};
        var retVal3 = findEquilibriumIndex(nums3);
    }
}
