namespace EnjoyAlgorithms.Module3;

internal class RomanToInteger
{
    int getValue(char ch)
    {
        switch (ch)
        {
            case 'I': return 1;
            case 'V': return 5;
            case 'X': return 10;
            case 'L': return 50;
            case 'C': return 100;
            case 'D': return 500;
            case 'M': return 1000;
        }
        return 0;
    }
    int romanToInt(string roman)
    {        
        int total = 0;
        
        for (int i = 0; i < roman.Length - 1; ++i)
        {
            int cur = getValue(roman[i]);
            int next = getValue(roman[i+1]);                        
            total += next > cur ? -cur : cur;
        }
        
        return total + getValue(roman[roman.Length - 1]);
    }

    public void Test()
    {
        var retVal1 = romanToInt("MCMIV");
    }
}