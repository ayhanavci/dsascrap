namespace EnjoyAlgorithms.Module3;

internal class ValidMountain
{
    //official
    bool validMountain(int []X, int n)
    {
        int climb = 0;

        //Find peak of the mountain
        while (climb < n - 1 && X[climb] < X[climb + 1])
            climb++;
        
        //Return false if peak is at the beginning or at the end
        if (climb == 0 || climb  == n - 1)
            return false;
        
        //Traverse down the mountain
        while (climb < n - 1 && X[climb] > X[climb + 1])
            climb++;
        
        //Return true if the end of the mountain is reached
        if (climb == n - 1)
            return true;
        else
            return false;
    }

    bool isValidMountain(int[] nums)
    {
        if (nums.Length < 3) return false;
        if (nums[0] >= nums[1]) return false;
        if (nums[nums.Length-1] >= nums[nums.Length-2]) return false;
        
        int pos = 2;

        for (; pos < nums.Length; ++pos) 
        {
            if (nums[pos] == nums[pos-1]) return false;
            if (nums[pos] < nums[pos-1]) break;
        }                            

        for (; pos < nums.Length; ++pos)
            if (nums[pos] >= nums[pos-1]) return false;
        
        return true;
    }
    public void Test()
    {
        int[] nums1 = {5,2,1,4};
        int[] nums2 = {5,8,8};
        int[] nums3 = {1,2,6,5,3};

        var retVal1 = isValidMountain(nums1);
        var retVal2 = isValidMountain(nums2);
        var retVal3 = isValidMountain(nums3);
    }
}