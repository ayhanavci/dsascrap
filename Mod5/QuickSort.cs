namespace EnjoyAlgorithms.Module5;

internal class QuickSort
{
    void Sort(int []nums, int l, int r)
    {            
        if (l >= r) return;

        int pivotIndex = Partition(nums, l, r);
        Sort(nums, l, pivotIndex - 1);
        Sort(nums, pivotIndex + 1, r);
    
    }
    int Partition(int []nums, int l, int r)
    {
        int pivot = nums[r];
        int i = l;        

        for (int j = l; j < r; ++j)
        {
            if (nums[j] < pivot)            
                Swap(nums, i++, j);                
            
        }
        Swap(nums, i, r);
        return i;
    }
    void Swap(int []nums, int i, int j)
    {        
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

    public void Test()
    {
        int []nums1 = {-7, 1, 5, 2, -4, 3, 0, -2};
        Sort(nums1, 0, 7);
    }
}
