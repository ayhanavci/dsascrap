namespace EnjoyAlgorithms.Module5;

internal class Sort012
{
    void Sort(int []nums)
    {
        int low = 0, mid = 0, high = nums.Length - 1;

        while (mid <= high)
        {
            if (nums[mid] == 0) Swap(nums, low++, mid++);
            else if (nums[mid] == 1) mid++;
            else Swap(nums, mid, high--);
        }
      
    }
    void Swap(int []nums, int i, int j)
    {        
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
    public void Test()
    {
        int []nums1 = {2, 1, 0, 0, 1, 2, 0};
        Sort(nums1);
    }
}
