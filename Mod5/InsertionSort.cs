namespace EnjoyAlgorithms.Module5;

internal class InsertionSort
{
    void Sort(int []nums)
    {            
        int len = nums.Length;        
        for (int i = 1; i < len; ++i)
        {                   
            int currValue = nums[i];
            int j = i - 1;

            while (j >= 0 && nums[j] > currValue)
            {
                nums[j + 1] = nums[j];
                j--;
            }
            nums[j + 1] = currValue;
        }

      
    }
    public void Test()
    {
        int []nums1 = {-7, 1, 5, 2, -4, 3, 0};
        Sort(nums1);
    }
}
