namespace EnjoyAlgorithms.Module5;

internal class MergeSort
{
    void Sort(int []nums, int l, int r)
    {            
        if (l == r) return;

        int mid = l + (r - l) / 2;
        Sort(nums, l, mid);
        Sort(nums, mid + 1, r);
        Merge(nums, l, mid, r);
    }
    void Merge(int []nums, int l, int mid, int r)
    {
        int n1 = mid - l + 1;
        int n2 = r - mid;
        int []A1 = new int[n1];
        int []A2 = new int[n2];
        
        for (int n = 0; n < n1; ++n)
            A1[n] = nums[l + n];

        for (int n = 0; n < n2; ++n)
            A2[n] = nums[mid + 1 + n];
        
        int i = 0, j = 0, k = l;

        while (i < n1 && j < n2)        
            if (A1[i] <= A2[j]) nums[k++] = A1[i++];
            else nums[k++] = A2[j++];
        
        while (i < n1) nums[k++] = A1[i++];

        while (j < n2) nums[k++] = A2[j++];

    }

    public void Test()
    {
        int []nums1 = {-7, 1, 5, 2, -4, 3, 0, -2};
        Sort(nums1, 0, 7);
    }
}
