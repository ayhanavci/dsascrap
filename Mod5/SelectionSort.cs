namespace EnjoyAlgorithms.Module5;

internal class SelectionSort
{
    void Sort(int []nums)
    {            
        int len = nums.Length;
        for (int i = 0; i < len - 1; ++i)
        {            
            int minIndex = i;                                    
            for (int j = i + 1; j < len; ++j)
            {
                if (nums[j] < nums[minIndex])
                    minIndex = j;
            }
            int tmp = nums[minIndex];
            nums[minIndex] = nums[i];
            nums[i] = tmp;
        }

      
    }
    public void Test()
    {
        int []nums1 = {-7, 1, 5, 2, -4, 3, 0};
        Sort(nums1);
    }
}
