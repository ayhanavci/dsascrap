namespace EnjoyAlgorithms.Module5;

internal class BubbleSort
{
    void Sort(int []nums)
    {            
        int len = nums.Length;
        for (int i = 0; i < len; ++i)
        {                                                
            for (int j = 0; j < len - i - 1; ++j)
            {
                if (nums[j] > nums[j+1])
                {
                    int tmp = nums[j];
                    nums[j] = nums[j+1];
                    nums[j+1] = tmp;
                }
            } 
        }

      
    }
    public void Test()
    {
        int []nums1 = {-7, 1, 5, 2, -4, 3, 0};
        Sort(nums1);
    }
}
