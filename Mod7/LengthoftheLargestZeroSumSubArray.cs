namespace EnjoyAlgorithms.Module7;

internal class LengthoftheLargestZeroSumSubArray
{
     int getLargestSum2(int []nums)
    {       
        Dictionary<int, int> H = new Dictionary<int, int>();
        int subArraySum = 0;
        int maxLength = 0;             
        for (int i = 0; i < nums.Length; ++i)
        {
            subArraySum += nums[i];
            if (subArraySum == 0)
                maxLength = Math.Max(maxLength, i + 1);
            else if (!H.ContainsKey(subArraySum)) 
                H.Add(subArraySum, i);
            else
                maxLength = Math.Max(maxLength, i - H[subArraySum]);

        }
        return maxLength;
    }
    int getLargestSum1(int []nums)
    {       
        int maxLength = 0;             
        for (int i = 0; i < nums.Length; ++i)
        {
            int currentSum = 0;
            for (int j = i; j < nums.Length; ++j)
            {
                currentSum += nums[j];
                if (currentSum == 0)
                    maxLength = Math.Max(maxLength, j - i + 1);
                
            }
        }
        return maxLength;
    }
    public void Test()
    {
        int []nums1 = {14, -1, 1, -6, 1, 5, 12, 17};
        int []nums2 = {1, 5, 10};
        int []nums3 = {1, 0, 2};
        var retVal1 = getLargestSum2(nums1);
        var retVal2 = getLargestSum2(nums2);
        var retVal3 = getLargestSum2(nums3);
    }
}
