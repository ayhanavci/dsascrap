using System.Collections;
using System.Text;
namespace EnjoyAlgorithms.Module7;

internal class SortCharactersByFrequency
{
    
    string sort(string str)
    {                    
        StringBuilder retVal = new StringBuilder();
        Dictionary<char, int> frequency = new Dictionary<char, int>();        
        Dictionary<char, int> firstOccurance = new Dictionary<char, int>();

        for (int i = 0; i < str.Length; ++i)
        {
            if (frequency.ContainsKey(str[i])) frequency[str[i]]++;
            else frequency.Add(str[i], 1);

            if (!firstOccurance.ContainsKey(str[i])) firstOccurance[str[i]] = i;            
        }
        while (frequency.Count > 0)
        {
            int maxOccurance = -1;
            char maxCh = '_';
            foreach (var freq in frequency)
            {
                char ch = freq.Key;
                int occurance = freq.Value;
                if (occurance > maxOccurance)
                {
                    maxCh = ch;
                    maxOccurance = occurance;                    
                }               
            }
            int pos = firstOccurance[maxCh];
            foreach (var occurChar in firstOccurance)
            {                
                if (maxOccurance == frequency[occurChar.Key] && occurChar.Value < pos)
                    maxCh = occurChar.Key;
            }
            for (int i = 0; i < maxOccurance; ++i)
                retVal.Append(maxCh);
            frequency.Remove(maxCh);
            firstOccurance.Remove(maxCh);
        }

        return retVal.ToString();
    }
    public void Test()
    {        
        var retVal1 = sort("tree");
        var retVal2 = sort("ccaaaAA");
    }
}
