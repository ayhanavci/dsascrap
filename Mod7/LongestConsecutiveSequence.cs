using System.Collections;
namespace EnjoyAlgorithms.Module7;

internal class LongestConsecutiveSequence
{
    int findLongest(int []nums)
    {                    
        HashSet<int> numberSet = new HashSet<int>();
        int longest = 0;
        
        foreach (var n in nums)
            numberSet.Add(n);
        
        foreach (var n in nums)
        {
            int curLength = 1;
            int check = n;
            while (numberSet.Contains(check-- - 1))
                ++curLength;
            longest = Math.Max(longest, curLength);
        }
        return longest;
    }
    public void Test()
    {
        int []nums1 = {4, 7, 1, 8, 10, 3};
        int []nums2 = {0, -3, 5, -1, 7, -2, -4, 1, 3};
        findLongest(nums1);
        findLongest(nums2);
    }
}
