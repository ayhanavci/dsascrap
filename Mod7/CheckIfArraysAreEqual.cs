using System.Collections;
namespace EnjoyAlgorithms.Module7;

internal class CheckIfArraysAreEqual
{
    bool check(int []nums1, int[] nums2)
    {                    
        Dictionary<int, int> numbers = new Dictionary<int, int>();

        foreach(var n in nums1)
            if (numbers.ContainsKey(n)) numbers[n]++;
            else numbers.Add(n, 1);
                
        for (int i = 0; i < nums2.Length; ++i)
        {
            if (!numbers.ContainsKey(nums2[i])) return false;
            if (numbers[nums2[i]] == 0) return false;
            numbers[nums2[i]]--;
        }
        return true;
    }
    public void Test()
    {
        int []nums1 = {0, 2, 5, 1, 2, 23};
        int []nums2 = {2, 0, 1, 23, 5, 2};
        check(nums1, nums2);
    }
}
