namespace EnjoyAlgorithms.Module7;

internal class FindMostFrequentElement
{
    int find(int []nums)
    {                    
        Dictionary<int, int> numbers = new Dictionary<int, int>();
        int maxFreq = -1;
        int answer = -1;

        foreach(var n in nums) 
        {   
            if (numbers.ContainsKey(n)) numbers[n]++;
            else numbers.Add(n, 1);
            
            int freq = numbers[n];
            if (freq > maxFreq)
            {
                maxFreq = freq;                
                answer = n;
            }
            else if (freq == maxFreq && n < answer)            
                answer = n;            
        }                            
        
        return answer;
    }
    public void Test()
    {
        int []nums1 = {2, 12, 1, 2, 8, 2, 2, 1, 8, 2};
        int []nums2 = {1, 9, 1, 1, 2};
        int []nums3 = {3, 8, 2, 3, 2};
        var retVal1 = find(nums1);
        var retVal2 = find(nums2);
        var retVal3 = find(nums3);
    }
}
