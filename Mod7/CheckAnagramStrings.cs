using System.Collections;
namespace EnjoyAlgorithms.Module7;

internal class CheckAnagramStrings
{
    bool check(string str1, string str2)
    {                    
        if (str1.Length != str2.Length) return false;

        Dictionary<char, int> str1Chars = new Dictionary<char, int>();
        Dictionary<char, int> str2Chars = new Dictionary<char, int>();
        
        for (int i = 0; i < str1.Length; ++i)
        {
            char ch = str1[i];
            if (str1Chars.ContainsKey(ch)) str1Chars[ch]++;
            else str1Chars.Add(ch, 1);

            ch = str2[i];
            if (str2Chars.ContainsKey(ch)) str2Chars[ch]++;
            else str2Chars.Add(ch, 1);
        }
            
        
        for (int i = 0; i  < str1Chars.Count; ++i)
        {
            if (!str2Chars.ContainsKey(str1[i])) return false;
            if (!str1Chars.ContainsKey(str2[i])) return false;

            if (str1Chars[str1[i]] != str2Chars[str1[i]]) return false;
            if (str2Chars[str2[i]] != str1Chars[str2[i]]) return false;
        }

        return true;
    }
    public void Test()
    {        
        check("dusty", "study");
        check("state", "taste");
        check("enjoyalgorithms", "enjoymathematics");
    }
}
